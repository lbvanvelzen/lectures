# Project 2 - Monte Carlo simulations

## Introduction

For the second project, you will choose *one* out of three different projects that
describe very different physics, but that are all tackled with one type of technique:
the Monte Carlo method. Your possible choices are:

1. **Monte-Carlo simulation of the Ising-model**<br>
   The Ising model is a simple model for (anti-)ferromagnetism. In
   this project you can study the physics of phase transitions,
   explore topics you learn in Advanced Statistical Mechanics (such as
   critical exponents), or implement advanced algorithms.
   
2. **Variational Quantum Monte Carlo**<br>
   In this project you will calculate the ground state energy of a
   Helium atom.  This is the simplest interacting quantum system (two
   electrons that not only feel the Coulomb interaction with the
   nucleus, but also amongst themselves), but cannot be solved
   analytically any more. You solve the problem on the computer
   approximately with a combination of Monte Carlo integration and the
   variational method.
   
3. **Monte Carlo simulation of polymers**<br>
   Polymers are long chains of atomic constituents. Like a thread of wool,
   they usually don't stay straight, but they curl up. This curling-up is
   influenced on how the consitutents of the polymer interact. In this
   project you will investigate these properties quantitatively.

## Instructions

### What we expect you to do

Again, work in groups of two (or three, after contact with the course team). You may
work with the same group as in project 1 or choose new partners.

Choose one of the three possible projects. As for the first project, we expect
you to write a well-structured simulation code, validate it, generate simulation
results in an organized fashion and summarize them in a proper scientific report.

Unlike the first project, we however give you more freedom and responsibility.
The lecture notes only give an introduction, and we expect you to study the details
on your own using the provided literature.

The second project is shorter in duration than the first project (4 weeks instead of 6)!
This is because (i) you are now more experienced but also (ii) because the second project
is actually less extensive than the first one.

### Milestones along the path

Like in the first project, we expect you to document your progress with respect to
weekly milestones. Unlike the first project however, we will only provide
you broad weekly goals. You will then first work out detailed milestones for
yourself, and then report your progress with respect to those.

## Resources

- Materials covered in the lectures. These are also summarized in the
  lecture notes.
- Literature for the *Monte Carlo simulation of the Ising model*:
    * The basics of the Ising model are explained in [Chapter 7](/downloads/Thijssen_Chapter7_Statistical_Mechanics.pdf) from the book "Computational Physics" by Jos Thijssen,
    * basics of the Monte Carlo simulation of the Ising model in [Chapter 10](/downloads/Thijssen_Chapter10_Monte_Carlo_Method.pdf)
    * details about advanced algorithms in [Chapter 15.5](/downloads/Thijssen_Chapter15_Methods_Lattice_Field_Theory.pdf).
- Literature for the *Variational Monte Carlo method*:
    * details on the variational method can be found in [Chapter 4 of the lecture notes of Applications of Quantum Mechanics](/downloads/aqm.pdf)
    * implementation details of the Monte Carlo method in [Chapter 12.2](/downloads/Thijssen_Chapter12_Quantum_Monte_Carlo.pdf)  from the book "Computational Physics" by Jos Thijssen.
- Literature for the *Monte Carlo simulation of polymers*:
    * details of the method in [Chapter 10.6](/downloads/Thijssen_Chapter10_Monte_Carlo_Method.pdf)
- Feel free to search for any help/code snippets/ideas you can find on
  the internet (but be sure to reference them properly!). We
  definitely encourage you to use Numpy/Scipy/...!

## Products

- Simulation code in a gitlab repository
- Report 
- Filled out weekly progress issues. These will be opened automatically in your
  gitlab repository, and need to be filled in before the next class.

## Assessment criteria

See the [details of the grading scheme here](proj2-grading.md)

## Supervision and help

* The lecturers and the TAs are present during the lectures and are
  willing to help with all your problems
* Out of class you are encouraged to ask any question on the course
  chat (preferred) or write an email to the lecturers.
  
## Submission and feedback

**Submission deadline for report and code: Sunday, 16 May 2021, 23:59.**

