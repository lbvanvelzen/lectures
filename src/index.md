# Computational Physics

!!! summary "Learning goals"

    After following this course you will be able to:

    - describe standard physical simulation algorithms.
    - implement these algorithms in a well-structured computer code.
    - apply these algorithms to calculate physical properties and interpret the outcome.
    - verify correctness of the simulation outcome and use this analysis to improve the code.
    - organize and perform joint programming work.
    - present the results according to the requirements of scientific communication.     - 

## Checklist for starting the course

Before starting the course, execute the following steps *in order*.

1. After you sign up for the course on brightspace, you will receive an invitation to the [course forum](https://forum.compphys.quantumtinkerer.tudelft.nl/) and the course [gitlab](https://gitlab.kwant-project.org/)—a version control server.
2. After you get an account, log in to the course computational environment at <https://hub.compphys.quantumtinkerer.tudelft.nl> (using the gitlab account from step 1).
3. If you want to follow the programming crash course, or you are otherwise interested
   in the material covered there, click [on this link](https://hub.compphys.quantumtinkerer.tudelft.nl/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.kwant-project.org%2Fcomputational_physics%2Fcomputing_crash_course&urlpath=lab%2Ftree%2Fcomputing_crash_course%2F)
4. [Follow the steps to start project 1](proj1-moldyn-start.md)

#### Notes on the lecture notes

In these notes our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/computational_physics/lectures), especially do [let us know](https://gitlab.kwant-project.org/computational_physics/lectures/issues/new?issuable_template=typo) if you see a typo!
