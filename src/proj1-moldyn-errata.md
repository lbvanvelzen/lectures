## Jos Thijssen, *Computational Physics*
- page 224:
   The expressions for the specific heat mentioned in the text are wrong. 
   The specific heat per atom for a solid in the high-temperature limit is
   $c_V = 3 k_B$ (Dulong-Petit law), and for a monatomic ideal gas $c_V = 3/2 k_B$.

## Loup Verlet, Physical Review 159, 98 (1967)
- equation (8) is missing a factor of 2. The expression for the pressure in the 
   [lecture notes](proj1-moldyn-week4#pressure) and in Jos Thijssen's book is correct.
