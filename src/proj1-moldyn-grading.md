# Grading scheme

For project 1, you will receive a grade for the repository (keeping track of your project
development), the submitted code and the submitted report.

Note that the relative weights given below are only an indication and are subject to
adjustment.

## Repository (30% of project grade):

- (70%) Weekly issues report planning, project progress with respect to the weekly
  milestones, backed up with data, code and plots.<br>
  For every week, you can get 2 (:smile:), 1 (:expressionless:), or 0 (:disappointed:)
  points, awarded by the respective emoji to the respective review issue.<br>
  If you fail to meet all milestones within a week, the grading is postponed by one week and the grade capped at 1 point.
  If you still fail to meet all milestones by then, you will get 0 points.
- (10%) Every project member makes regular commits<br>
  "Regular" means that subsequent commits by one person shold not be more than two weeks
  apart.<br>
  Make sure every group member makes regular commits, this is graded by member, not
  project. Also, if a group member has no commits at all, he/she will not receive a grade
  for this project.
- (10%) Commit messages are descriptive
- (10%) No unnecessary/temporary/duplicate files

## Code (30% of the project grade):
* Critical
    - The code works
* Important
    - Simulation is separate from execution and data processing (performed in different modules or module + notebook)
    - The only global variables are physical or simulation constants
    - The code is organized according to the simulation logic and uses functions (or classes)
    - When possible and reasonable, efficient functions from scipy stack libraries are used
* Minor
    - Docstrings correctly explain how to use each function
    - Consistent formatting and naming conventions (e.g. following PEP8)
    - Nontrivial (and only nontrivial) parts of code are commented
    - Variable and function names are understandable and follow a logical scheme
* Minor penalty (0.5 points):
    - Mixing different spoken languages (we recommend English-only)


## Report (40% of the project grade):
*  Major:
    - Explains the theoretical background
    - Documents correctness checks
    - Data is reported with uncertainty, and fitted to the expected behavior if applicable
    - A variety of phenomena are investigated (varies per project), and compared with known results
*  Minor:
    - Performance is reported and discussed
    - Individual contributions and work organization are documented
    - Figures and data presentation fulfill publication standards
    - Report is as a short paper with abstract, introduction, methods, results, conclusions and references.