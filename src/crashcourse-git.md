# Version control with git

## Why version control?

Many of you will recognize this situation:

![](figures/geek-poke-vcs.jpeg)

A proper version control system should

* allow you to keep track of changes in your files
* give access to previous versions
* enable you to work with others together

Using a version control system will be crucial when you work together
as a group on a project. In this course, we use the `git` version control
system. There are different ways to use `git`. In the following videos
we will show you what concepts are behind version control, and how you can
use `git` through a graphical user interface on [the compute hub](https://hub.compphys.quantumtinkerer.tudelft.nl).

## What is version control? A manual exercise

<iframe width=100% height=540 src="https://www.youtube-nocookie.com/embed/eQfrs-YTvv0?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## `git` - a distributed version control

<iframe width=100% height=540 src="https://www.youtube-nocookie.com/embed/RFp7eNkbWBE?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Setting up a git repository

<iframe width=100% height=540 src="https://www.youtube-nocookie.com/embed/PLfQSdmqZ4I?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Working on your own with git

<iframe width=100% height=540 src="https://www.youtube-nocookie.com/embed/FC1RGyGrhhI?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Working with others - no conflicts

<iframe width=100% height=540 src="https://www.youtube-nocookie.com/embed/DlVQdi39lJA?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Working with others - conflicts

<iframe width=100% height=540 src="https://www.youtube-nocookie.com/embed/txk5044Iz9Q?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Miscellaneous git topics

<iframe width=100% height=540 src="https://www.youtube-nocookie.com/embed/d4DorY5u6So?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Outlook

The videos are focused on the `jupyterlab-git` plugin. However, the concepts
we show are general, and you should be able to transfer these to other
interfaces to `git`.