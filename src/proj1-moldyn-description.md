# Project 1 - Molecular dynamics simulation of Argon atoms

## Introduction

In your daily life you encounter different phases of matter all the
time: Water can be in the form of ice covering lakes and canals, it is
fluid when it comes out of the tap, and it turns to vapor when you boil
it.

In this assignment you will write a simulation code to explore
different phases of matter quantitatively for a simpler system: Argon
atoms (water is surprisingly complicated and still an on-going field
of research). Through these simulations you will be able to experience
the physics of different phases and investigate their behavior in a
systematic and quantitative way.

## Instructions

### What we expect you to do

You will work on this assignment in a group. We recommend to work in pairs,
but groups of three are also permissible.

Write a well-structured computer code performing a molecular dynamics
simulation of Argon atoms. This code should be flexible and allow
for different ambient conditions (such as density or temperature) to
explore different phases of Argon. The recommended programming language
for the assignment is Python, but you may use any computer language you like.

Validate the correctness of your code, and run simulations for Argon
in different phases. Investigate these phases quantitatively, for
example using pair correlation functions, calculating pressure or
specific heat, or exploring diffusion properties. Note that while
there is a standard set of things you *can* do, the assignment is
completely open to your creativity. We definitely encourage you to
also explore things according to your own ideas an surprise us!

You will then summarize, your findings in a report that is written
according to scientific standards.

### Milestones along the path

Writing a molecular dynamics simulation code is of course a formidable
task - we will guide in the lectures towards this goal, by covering
the elements you should focus on first. Along this way you will most
certainly go astray and possibly make wrong choices - this is an
integral part of the learning experience. However, we will be there to
help you get back on track!

In particular, we have defined *milestones* for every week. Each week,
you will write a short text about the progress in your project, motivating
and validitating your choices. You should use the milestones of every week
as the reference.

If you fail to meet all milestones in one week, this is fine. Try to make up
for it in the next week. If you fall behind two weeks or more, you are in danger
of not finishing in time!

## Resources

- Materials covered in the lectures. These are also summarized in the
  lecture notes.
- [Chapter 7](/downloads/Thijssen_Chapter7_Statistical_Mechanics.pdf) and 
  [chapter 8](/downloads/Thijssen_Chapter8_Molecular_Dynamics.pdf) from the book
  "Computational Physics" by Jos Thijssen.
- [The original paper by Verlet](/downloads/Verlet.pdf) on the molecular dynamics simulation of
  Argon.
- We have collected a [list of errata](proj1-moldyn-errata.md) for Jos' book and Verlet's paper.
- Feel free to search for any help/code snippets/ideas you can find on
  the internet - but make sure to properly acknowledge this. We definitely encourage you to use Numpy/Scipy/...!

## Products

- Simulation code in a gitlab repository
- Report 
- Filled out weekly progress issues. These will be opened automatically in your
  gitlab repository, and need to be filled in before the next class.

## Assessment criteria

See the [details of the grading scheme here](proj1-moldyn-grading.md)

## Supervision and help

* The lecturers and the TAs are present during the lectures and are
  willing to help with all your problems, such as debugging, advising what is
  the best way to solve a specific programming problem, ...
* Out of class you are encouraged to ask any question on the course
  forum (preferred) or write an email to the lecturers.
  
## Submission and feedback

**Submission date for report and code: 21 March 2021, 23:59**
