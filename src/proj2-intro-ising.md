# Monte Carlo simulations for the Ising model

The Ising model is a simple model for (anti)-ferromagnetism. In 2D, it
consists of spins sitting on a square lattice, where the spins $s_i$
can take values $+1$ or $-1$. The Hamiltonian is given as

$$
H = - J \sum_{\langle ij\rangle} s_i s_j - H \sum_i s_i
$$

where $\langle i j \rangle$ denotes a sum over nearest neighbors
(horizontal and vertical) on the lattice. $J>0$ favors
ferromagnetism, $J<0$ antiferromagnetism, and $H$ represents an
external magnetic field.

The 2D Ising model can actually [be solved
analytically](https://en.wikipedia.org/wiki/Square-lattice_Ising_model),
and shows a phase transition from non-magnetic to ferromagnetic (if
$J>0$) as a function of temperature.

Write a Monte Carlo simulation to study the thermodynamic properties
of a $n\times n$ lattice. This can be done using the [Metropolis
Monte Carlo algorithm introduced in the
lecture](proj2-monte-carlo.md#metropolis-monte-carlo). A Monte Carlo move in
the simplest case would consist of picking a spin and flipping its
values. (This is good as a starting point, but more advanced
algorithms are available, see below).

Of particular interest is to compute the average absolute value of the
magnetization $\langle \left|m\right|\rangle = \frac{1}{N} \langle
\left|\sum_i s_i\right|\rangle$, the specific heat per spin $c =
\frac{k_B \beta^2}{N} \left( \langle E^2 \rangle - \langle E \rangle^2
\right)$, and the susceptibility per spin $\chi = \beta N \left(
\langle m^2 \rangle - \langle m \rangle^2 \right)$ (use your
thermodynamics knowledge to convince you that those formulas are
correct).

As a first step, write a simple Monte Carlo program computing the
average magnetization and $\chi$ or $c$ as a function of temperature
to find the (analytically known) phase transition. In this basic
simulation, a Monte Carlo move consists of attempting to flip a single
spin (convince yourself that this move has equal probability for going
forth and back, as required by detailed balance!), followed by acceptance
or rejection according to the change in energy.

After that, you can extend the simulation in different directions to
improve on these results, for example by:

- implementing the Swendsen-Wang or Wolff algorithm. The conventional
  Metropolis algorithm suffers from critical slow-down near the phase
  transition, these more advanced algorithms overcome this limitation.
- implementing a numpified version of the Metropolis algorithm, making use
  of the checkerboard decomposition (i.e. assign the spins on the square lattice
  a "black" and "white" tag as in a checkerboard). You can then update all
  the "black" spins in one go with numpy, followed by all the "white" spins.
- study critical exponents of the Ising model making use of finite-size scaling

In this project, there is quite some freedom in which direction you can go -
what is listed above are just some examples. Make a choice!

## Milestones

In the second project, we only provide you high-level milestones for every
week! You will need to define every week your own detailed milestones

- **Week 1**: Start to implement a basic simulation of the Ising model using the Metropolis algorithm, argue why your approach fulfils detailed balance, and make plans how
to validate your code.
- **Week 2**: Finish the basic simulation and validate your results
- **Week 3**: Generate results (including error bars) and start to implement
one of the possible extensions (Swendsen-Wang/Wolff algorithm, numpified Metropolis
algorithm, *or* studying critical exponents. You may also choose a different direction,
but then make sure to discuss with us)

## Literature

Study the provided literature for the details you need to understand:

- the basics of the Ising model are explained in [Chapter 7](/downloads/Thijssen_Chapter7_Statistical_Mechanics.pdf) from the book "Computational Physics" by Jos Thijssen,
- basics of the Monte Carlo simulation of the Ising model in [Chapter 10](/downloads/Thijssen_Chapter10_Monte_Carlo_Method.pdf)
- details about advanced algorithms in [Chapter 15.5](/downloads/Thijssen_Chapter15_Methods_Lattice_Field_Theory.pdf)
- there is a huge amount of (pedagogic) literature for the Ising model. Find your own
  sources, too!