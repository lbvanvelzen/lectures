# Introduction to basic python

## Lecture video

<iframe width=100% height=420 src="https://www.youtube-nocookie.com/embed/DbrKx1zf3d4?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Exercise material

[Notebooks introducing basic python, with simple exercises and solutions](https://gitlab.kwant-project.org/computational_physics/computing_crash_course/-/tree/master/day1_morning)

(part of the [computing crash course material](https://gitlab.kwant-project.org/computational_physics/computing_crash_course))