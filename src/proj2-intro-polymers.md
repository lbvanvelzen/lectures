# Monte Carlo simulations of polymers

This project is a bit different from the other two on the technical side:
instead of using the Metropolis algorithm (i.e. generating a Markov chain)
to sample the Boltzmann distribution, we use a [nearby distribution for the
importance sampling](proj2-monte-carlo.md#importance-sampling)

The goal is to simulate the properties of long polymers in a good
solvent (the latter just means that the polymers do not want to stick
to other polymers in the solution, and thus we can simulate individual
polymers), and to study how they curl up.

To do this, we use a simple model for the polymer in 2D

- it consists of a sequence of $N$ consecutive "beads"
- the distance between beads is fixed (1 in dimensionless units)
- the angle $\theta$ between the links of three neighboring beads can take
  any value
- the interaction between the beads/atoms is modelled with a Lennard-Jones
  potential

If there was no interaction between atoms, the polymer would correspond to
a random walk. The repulsive interaction of the Lennard-Jones potential
prevents atoms to come too close and turns the polymer into a *self-avoiding
random walk*.

![](/figures/build_polymer.svg)

We could sample the different polymers by constructing a full polymer
with $N$ beads by chosing $N-1$ angles
completely randomly. The problem with this approach is that most
polymers would energetically be very unfavorable. The Rosenbluth
algorithm improves on this by building up a polymer step by step: a
new bead with angle $\theta$ is added to an existing polymer with
probability $\sim \exp(-E(\theta)/k_b T)$, where $E(\theta)$ is
the additional energy due to the extra bead. Note that while this looks like
the Boltzmann distribution, the full polymer does not go according to the
Boltzmann distribution! The Monte Carlo sampling
then consists of growing an ensemble of polymers (which are completely
independent, unlike the Markov chain).

To account for the deviation from the real Boltzmann distribution , we
have to associate a weight factor $W_i$ with the $i$-th sample.
The expectation value of a quantity $A$ is then given by

$$
\langle A\rangle = \frac{\sum_i W_i A_i}{\sum_i W_i}
$$

Work out the weight factors by consulting [Chapter 10.6 from Jos'
book!](/downloads/Thijssen_Chapter10_Monte_Carlo_Method.pdf). It is essential to
get these right, otherwise your simulation will not work.

To quantitatively investigate the curling of polymers, it is useful t
the end-to-end distance of the polymers. Compare the case with
interactions to the case without interactions between the atoms.

After implementing the Rosenbluth algorithm, you will find that for
long polymers very few samples have a large weight and dominate,
giving a big error on the result. You can fix this problem by using a
more advanced sampling method, called PERM. Implement this as the
final step and show that PERM is indeed superior to Rosenbluth for
long polymers.

This project is again rather predefined, and what is described above is
the natural route for the project. If you have an additional idea,
feel free to discuss with us!

## Milestones

In the second project, we only provide you high-level milestones for every
week! You will need to define every week your own detailed milestones

- **Week 1**: Start to implement a basic simulation of polymers using the Rosenbluth algorithm, explain the weights entering the simulation, and make plans how to validate your code.
- **Week 2**: Finish the basic simulation and validate your results
- **Week 3**: Generate results (including error bars) and start to implement the PERM
algorithm. 


## Literature

- details of the method in [Chapter 10.6](/downloads/Thijssen_Chapter10_Monte_Carlo_Method.pdf)