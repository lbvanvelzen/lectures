# The Monte Carlo method

??? info "Lecture video"

    <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/a6NqGjkCkk0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Introduction

Lecture notes will be published soon!

## Monte Carlo integration

### A simple example

### Why Monte Carlo integration becomes beneficial for high-dimensional integrals


## Importance sampling


## Metropolis Monte Carlo

**Summary of Metropolis algorithm**

1. Start with a state $X_i$
2. Generate a state $X'$ from $X_i$ (such that $\omega_{X_i,X'}=\omega_{X',X_i}$)
3. If $p(X') > p(X_i)$, set $X_{i+1}=X'$ <br>
   If $p(X') < p(X_i)$, set $X_{i+1}=X'$ with probability $\frac{p(X')}{p(X)}$<br>
   else set $X_{i+1} = X_i$.
4. Continue with 2.