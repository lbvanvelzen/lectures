# Starting project 1

- Projects in this course are done in groups of 2 (or in exceptional cases 3) students.
  Please decide on your partner(s) before the first class.
- We use [GitLab](gitlab.kwant-project.org) for the bulk of the communication on your
  project. To start and register project 1 with us, go to [this link](https://gitlab.kwant-project.org/computational_physics/organization/issues/new?issuable_template=create_project_md)
  and follow the instructions.
- A new git project is now created for you. We taught how to use `git` in the computing
  crash course. The material used there is also useful for self-study.
- Make sure you read the [project description](proj1-moldyn-description.md) and the
  [grading scheme](proj1-moldyn-grading.md) to make sure you are aware of what we
  expect from you
- In particular, be aware that a part of your grade is based on tracking the progress
  in the project (consult the file `journal.md` in your git repository for details).
  In this first project, we take you by the hand with detailed milestones. In later
  projects we gradually expect more independence.
