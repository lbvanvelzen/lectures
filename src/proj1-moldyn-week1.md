# Project 1: Molecular dynamics

Writing a molecular dynamics simulation is of course a formidable task. During 
the lectures, we will guide you along the way towards writing a full-fledged 
simulation. At the end of the lecture notes of each week we list a set of milestones
to aid you. These milestones also allow us to follow your progress, which helps
in supporting you. These lecture notes flesh out a lot of the questions 
arising on the way to these milestones

  
### Project goals

In this project we want to simulate phase transitions in materials: under which circumstances is the material a solid, a liquid or a gas?

In order to do this, we will simulate the classical motion of particles (for simplicity) in the material. The first choice is which material to simulate. Water, for example, is very complicated. It forms hydrogen bonds, the molecule is very asymmetric, etc. Considering we want something simpler, a noble gas is a very reasonable choice. Mainly because one doesn't have to worry about the formation of molecules. Historically, Argon (Ar) is the best studied and this is what we will do, too.

In a simulation, we will have to consider different aspects:

### How do Argon atoms interact?

Argon atoms are neutral, so they do not interact via Coulomb interaction. However, small displacements between the nucleus and the electron cloud gives rise to a small dipole moment. Overall, the interaction between dipoles gives an attractive interaction that scales as $U(r) \propto 1/r^6$ where $r$ is the distance between two atoms.

On the other hand, Argon atoms do not come so close to each other that they overlap (they do not form molecules), hence there must be a repulsive force at small distances. 

Both the attractive and repulsive interaction are parametrized in the Lennard-Jones potential, which takes the form:

$$
U(r) = 4\epsilon \left(\left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^6 \right)
$$

For Argon, the fitting parameters have been determined as $\epsilon/k_B=119.8\,\mathrm{K}$ and $\sigma = 3.405\,\text{Angstrom}$.

### In which space do the Argon atoms sit?

We want to simulate an infinite system of Argon atoms - but obviously, a computer cannot simulate infinite space.

In practice, we will thus use a finite box of length $L$, with periodic boundary conditions (to emulate an infinite system). Recall that a periodic boundary condition for a wave function means: $\psi(x + L) = \psi(x)$, where $L$ is the size of the one-dimensional system. For a molecular dynamics simulation, we choose a periodic boundary condition to ensure that an atom will never reflect of a hard boundary. This has some subtle effects on the molecular dynamics simulation:

- If a particle leaves the box, it re-enters at the opposite side
- The evaluation of the interaction between two atoms is also influenced by the periodic boundary condition: the shortest distance between atoms may now include crossing the system boundary and re-entering. This is identical to introducing copies of the particles around the simulation box:

![week1gif](figures/gif_week_1.gif)
The left part of the animation shows the actual simulation box, while the right part illustrates how the periodic boundary condition is implemented. The simulation box is now the middle square in the right part of the animation, the other 8 squares are copies. So a particle close to the far right system boundary is actually close to the left boundary too. As a result, the smallest distance between particles may thus not be the distance within the box! Here we use the convention to choose the pair of atoms that has the shortest distance. This is modeled by introducing copies of the particles in volumes bordering the actual system.
  
### How do the particles evolve in time?

The motion of the particles is governed by Newton's equation. Hence, the particle $i$ will behave as follows:

$$
m \frac{d^2\mathbf{x}_i}{dt^2} = \mathbf{F}(\mathbf{x}_i) = - \nabla U(\mathbf{x}_i)
$$

(a little reminder: when we have a potential that only depends on the distance $r=\sqrt{x^2+y^2+z^2}$, we have
$\nabla U(r) = \frac{dU}{dr} \frac{\mathbf{x}}{r}$)

These equations cannot be solved exactly, but need to be numerically solved approximately. Instead of continuous time, we thus will evolve the particles with finite time steps $h$.

A very simple way of doing this is to use the Euler method. If $\mathbf{x}_i(t_n)$ and $\mathbf{v}_i(t_n)$ are the position and velocity of particle $i$ at time $t_n$, the position and velocity at the next time $t_{n+1} = t_n + h$ are given by:

$$
\begin{aligned}
\mathbf{x}_i(t_{n+1}) & = \mathbf{x}_i(t_{n}) + \mathbf{v}_i(t_{n}) h \\
\mathbf{v}_i(t_{n + 1}) & = \mathbf{v}_i(t_{n}) + \frac{1}{m} \mathbf{F}(\mathbf{x}_i(t_{n})) h
\end{aligned}
$$

### What to do

Play around with this system. Start by simulating the time evolution of a few particles in a periodic box, add the forces due to the Lennard-Jones potential.

It's easier to start with a 2D system, but plan to switch to 3D at a later stage.

### Milestones
Here are a number of things you would need to achieve to satisfy the goals of this first week of the molecular dynamics project. At each milestone, think how you yourself would verify whether this is working or not. As stated above, start simple. Two dimensions and a small (2 is enough) number of atoms keeps your simulation orderly. This enables you to pinpoint errors more easily, as opposed to a three-dimensional simulation with 100 atoms. 

!!! check "Milestones"
    - Develop a way to store each particle's velocity and position at every timestep
    - Calculate the force on each particle using the Lennard-Jones potential
    - Implement the Euler method for time evolution
    - Implement the periodic boundary condition
    - Define a function that calculates the total energy of the system
    - Make sure that you commit your code regularly to Gitlab.